# Gitaly shard project repository balancer

Should detect overloaded gitaly shards and schedule the movement of 1000 GB
worth of project repositories from each overloaded shard to whichever shard
is selected by the shard weights configured automatically in the gitlab
application.

To be clear, a shard is just a gitaly node.  A gitaly node is just a host
running Ubuntu with the gitaly application running as a service.  The
gitaly application acts as an abstraction layer for git repository
operations required for GitLab features. Each gitaly node holds some of
the project repository inventory managed by the gitlab application
instance.


## Setup

```bash
asdf install ruby
gem install bundler
bundle install
```


## Invocations


### Example shard selection

```bash
rm -f ./shards.json
bundle exec lib/shards.rb > ./shards.json
cat shards.json | jq
```

```json
{
  "shards": [
    "nfs-file01"
  ]
}
```


### Example project selection

```bash
export GITLAB_GSTG_ADMIN_API_PRIVATE_TOKEN='changeme'
rm -f ./projects.json
bundle exec lib/projects.rb nfs-file01 --limit=2 > ./projects.json
cat projects.json | jq
```

```json
{
  "projects": [
    {
      "id": 1,
      "path_with_namespace": "namespace/project_name_01",
      "human_readable_size": "1.72 GB",
      "repository_storage": "nfs-file01"
    },
    {
      "id": 2,
      "path_with_namespace": "namespace/project_name_02",
      "human_readable_size": "1.67 GB",
      "repository_storage": "nfs-file01"
    }
  ]
}
```


### Example project move

```bash
bundle exec lib/mover.rb --json=./projects.json --limit=1 --dry-run=yes
2020-11-11 00:05:55 INFO  [Dry-run] This is only a dry-run -- write operations will be logged but not executed
2020-11-11 00:05:55 INFO  Will move 1 projects
2020-11-11 00:05:57 INFO  ======================================================
2020-11-11 00:05:57 INFO  Scheduling repository move for project id: 1
2020-11-11 00:05:57 INFO    Project path: namespace/project_name_01
2020-11-11 00:05:57 INFO    Current shard name: nfs-file01
2020-11-11 00:05:57 INFO    Repository size: 1.72 GB
2020-11-11 00:05:57 INFO  [Dry-run] Would have scheduled repository move for project id: 1
2020-11-11 00:05:57 INFO  ======================================================
2020-11-11 00:05:57 INFO  Done
2020-11-11 00:05:57 INFO  [Dry-run] Would have processed 1.72 GB of data
```


## Pipeline stages

`TODO`

## Schedule

For the `.com` environment:

```bash
rm -f ./pipeline.json
export project_id=22372077
# Create pipeline schedule:
curl --silent --show-error --location --request POST --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" --form description="Balance shards [staging]" --form ref="main" --form cron="0 1 * * *" --form cron_timezone="UTC" --form active="true" "https://gitlab.com/api/v4/projects/${project_id}/pipeline_schedules" > ./pipeline.json
export pipeline_id=$(cat ./pipeline.json | jq -r '.["id"]')
# Create pipeline schedule variables:
curl --silent --show-error --location --request POST --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" --form "key=SCHEDULED_EXECUTION" --form "value=true" "https://gitlab.com/api/v4/projects/${project_id}/pipeline_schedules/${pipeline_id}/variables" | jq
```

For the `gprd` environment:

```bash
rm -f ./pipeline.json
export project_id=22418805
# Create pipeline schedule:
curl --silent --show-error --location --request POST --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" --form description="Balance shards [staging]" --form ref="main" --form cron="0 1 * * *" --form cron_timezone="UTC" --form active="true" "https://gitlab.com/api/v4/projects/${project_id}/pipeline_schedules" > ./pipeline.json
export pipeline_id=$(cat ./pipeline.json | jq -r '.["id"]')
# Create pipeline schedule variables:
curl --silent --show-error --location --request POST --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" --form "key=SCHEDULED_EXECUTION" --form "value=true" "https://gitlab.com/api/v4/projects/${project_id}/pipeline_schedules/${pipeline_id}/variables" | jq
```

For the `ops` environment:

```bash
rm -f ./pipeline.json
export project_id=419
# Create pipeline schedule:
curl --silent --show-error --location --request POST --header "PRIVATE-TOKEN: ${GITLAB_OPS_API_PRIVATE_TOKEN}" --form description="Balance shards [staging]" --form ref="main" --form cron="0 1 * * *" --form cron_timezone="UTC" --form active="true" "https://ops.gitlab.net/api/v4/projects/${project_id}/pipeline_schedules" > ./pipeline.json
export pipeline_id=$(cat ./pipeline.json | jq -r '.["id"]')
# Create pipeline schedule variables:
curl --silent --show-error --location --request POST --header "PRIVATE-TOKEN: ${GITLAB_OPS_API_PRIVATE_TOKEN}" --form "key=SCHEDULED_EXECUTION" --form "value=true" "https://gitlab.com/api/v4/projects/${project_id}/pipeline_schedules/${pipeline_id}/variables" | jq
```

## Manifest

```bash
$ tree -I .git -I vendor
.
├── Gemfile
├── Gemfile.lock
├── README.md
├── lib
│   ├── mover.rb
│   ├── projects.rb
│   └── shards.rb
├── log
├── shards.promql
└── storage_migrations

3 directories, 7 files
```

#! /usr/bin/env ruby
# frozen_string_literal: true

# vi: set ft=ruby :

# -*- mode: ruby -*-

require 'json'
require 'logger'
require 'net/http'
require 'optparse'
require 'ostruct'
require 'uri'

# Storage module
module Storage
  # ShardSelectorScript module
  module ShardSelectorScript
    LOG_TIMESTAMP_FORMAT = '%Y-%m-%d %H:%M:%S'
    TWO_WEEKS_AGO_IN_SECONDS = 2 * 7 * 24 * 60 * 60
    SIMULATED_SHARDS = %w[nfs-file01 nfs-file09].freeze

    # Config module
    module Config
      DEFAULTS = {
        log_level: Logger::INFO,
        environment: :staging,
        shards_prometheus_query_file_path: 'shards.promql',
        api_endpoints: {
          staging: 'http://thanos-query-01-inf-ops.c.gitlab-ops.internal:10902/api/v1',
          production: 'http://thanos-query-01-inf-ops.c.gitlab-ops.internal:10902/api/v1'
        },
        prometheus_environment_identifiers: {
          staging: 'gstg',
          production: 'gprd'
        },
        query_api_uri: 'query',
        excluded: [],
        limit: -1,
        simulate: false,
        simulated_shards: SIMULATED_SHARDS,
        threshold: 0.147
      }.freeze
    end
  end
  # module ShardSelectorScript

  class UserError < StandardError; end
  class Timeout < StandardError; end
end

# Re-open the Storage module to add the Logging module
module Storage
  # This module defines logging methods
  module Logging
    LOG_FORMAT = "%<timestamp>s %-5<level>s %<msg>s\n"

    def formatter_procedure(format_template = LOG_FORMAT)
      proc do |level, t, _name, msg|
        format(
          format_template,
          timestamp: t.strftime(::Storage::ShardSelectorScript::LOG_TIMESTAMP_FORMAT),
          level: level, msg: msg)
      end
    end

    def initialize_log(formatter = formatter_procedure)
      STDOUT.sync = true
      log = Logger.new(STDOUT)
      log.level = Logger::INFO
      log.formatter = formatter
      log
    end

    def log
      @log ||= initialize_log
    end

    HEADER_ARGUMENT_TEMPLATE = '--header "%<field>s: %<value>s"'

    def get_request_headers(request)
      request.instance_variable_get('@header'.to_sym) || []
    end

    # rubocop: disable Style/PercentLiteralDelimiters
    # rubocop: disable Style/RegexpLiteral
    def to_unescaped_json(data)
      return data unless data.respond_to?(:to_json)

      data.to_json.gsub(%r[\\"], '"').gsub(%r[^"{], '{').gsub(%r[}"$], '}')
    end
    # rubocop: enable Style/PercentLiteralDelimiters
    # rubocop: enable Style/RegexpLiteral

    # This method reproduces a Net:HTTP request as a verbatim
    # port to a curl command, executable on the command line,
    # in order to make a given web-request portable.
    def debug_request(request)
      headers = get_request_headers(request)
      log.debug "[The following curl command is for external diagnostic purposes only:]"
      curl_command = "curl --verbose --silent --compressed ".dup
      curl_command = curl_command.concat("--request #{request.method.to_s.upcase} ") if request.method != :get
      curl_command = curl_command.concat("'#{request.uri}'")
      header_arguments = headers.collect do |field, values|
        format(HEADER_ARGUMENT_TEMPLATE, field: field, value: values.join(','))
      end
      unless header_arguments.empty?
        curl_command = curl_command.concat(' ')
        curl_command = curl_command.concat(header_arguments.join(' '))
      end
      body = request.body
      curl_command = curl_command.concat(" --data '#{to_unescaped_json(body)}'") unless body.nil? || body.empty?
      log.debug curl_command
    end
  end
  # module Logging
end
# module Storage

# Re-open the Storage module to add the Helpers module
module Storage
  # Helper methods
  module Helpers
    SHARD_APP_NAME_TEMPLATE = 'nfs-%<shard>s'

    def get_api_url(resource_path)
      raise 'No such resource path is configured' unless options.include?(resource_path)

      endpoints = options[:api_endpoints]
      environment = options[:environment]
      endpoint = endpoints.include?(environment) ? endpoints[environment] : endpoints[:production]
      abort 'No api endpoint url is configured' if endpoint.nil? || endpoint.empty?

      [endpoint, options[resource_path]].join('/')
    end

    def symbolize_keys_deep!(memo)
      memo.keys.each do |key|
        symbolized_key = key.respond_to?(:to_sym) ? key.to_sym : key
        memo[symbolized_key] = memo.delete(key) # Preserve order even when key == symbolized_key
        symbolize_keys_deep!(memo[symbolized_key]) if memo[symbolized_key].is_a?(Hash)
      end
    end

    def password_prompt(prompt = 'Enter password: ')
      $stdout.write(prompt)
      $stdout.flush
      $stdin.noecho(&:gets).chomp
    ensure
      $stdin.echo = true
      # $stdout.flush
      $stdout.ioflush
      $stdout.write "\r" + (' ' * prompt.length) + "\n"
      $stdout.flush
    end
  end
end

# Re-open the Storage module to add the CommandLineSupport module
module Storage
  # Support for command line arguments
  module CommandLineSupport
    # OptionsParser class
    class OptionsParser
      include ::Storage::Helpers
      attr_reader :parser, :options

      def initialize
        @parser = OptionParser.new
        @options = ::Storage::ShardSelectorScript::Config::DEFAULTS.dup
        define_options
      end

      def define_options
        @parser.banner = "Usage: #{$PROGRAM_NAME} [options]"
        @parser.separator ''
        @parser.separator 'Options:'
        define_thanos_option
        define_limit_option
        define_excluded_option
        define_simulate_option
        define_env_option
        define_verbose_option
        define_tail
      end

      def define_thanos_option
        environment = @options[:environment]
        api_endpoints = @options[:api_endpoints]
        thanos_endpoint = api_endpoints[environment]
        description = "Thanos endpoint; default: #{thanos_endpoint}"
        @parser.on('-T', '--thanos=<thanos_endpont>', description) do |thanos_endpont|
          @options[:api_endpoints][environment] = thanos_endpont
        end
      end

      def define_limit_option
        description = "Selection limit; default: #{@options[:limit].negative? ? 'no limit' : @options[:limit]}"
        @parser.on('-l', '--limit=<n>', Integer, description) do |limit|
          @options[:limit] = limit
        end
      end

      def define_excluded_option
        description = "Shard exclusions; default: #{@options[:excluded].empty? ? 'none' : @options[:excluded]}"
        @parser.on('-x', '--excluded=<shard>,...', Array, description) do |excluded|
          @options[:excluded] = excluded
        end
      end

      def define_simulate_option
        description = 'Simulate shard selection; default: no'
        @parser.on('-S', '--simulate=[yes/no]', description) do |simulate|
          @options[:simulate] = simulate.match?(/^(yes|true)$/i)
        end
      end

      def validated_env(env)
        env = env.to_sym
        raise OptionParser::InvalidArgument, "Invalid environment: #{env}" unless @options[:api_endpoints].key?(env)

        env
      end

      def define_env_option
        description = "Operational environment; default: #{@options[:environment]}"
        @parser.on('--environment=<staging|production>', description) do |env|
          @options[:environment] = validated_env(env)
        end
      end

      def define_verbose_option
        @parser.on('-v', '--verbose', 'Increase logging verbosity') do
          @options[:log_level] -= 1
        end
      end

      def define_tail
        @parser.on_tail('-?', '--help', 'Show this message') do
          puts @parser
          exit
        end
      end
    end
    # class OptionsParser

    def demand(options, arg, type = :positional)
      return options[arg] unless options[arg].nil?

      required_arg = "<#{arg}>"
      required_arg = "--#{arg.to_s.gsub(/_/, '-')}" if type == :optional
      raise UserError, "Required argument: #{required_arg}"
    end

    def parse(args = ARGV, file_path = ARGF)
      opt = OptionsParser.new
      opt.parser.parse!(args)
      opt.options
    rescue OptionParser::InvalidArgument, OptionParser::InvalidOption,
           OptionParser::MissingArgument, OptionParser::NeedlessArgument => e
      puts e.message
      puts opt.parser
      exit
    rescue OptionParser::AmbiguousOption => e
      abort e.message
    end
  end
  # module CommandLineSupport
end
# module Storage

# Re-open the Storage module to define the ThanosClient class
module Storage
  # Define the ThanosClient class
  class ThanosClient
    DEFAULT_RESPONSE = OpenStruct.new(code: 400, body: '{}') unless defined? ::Storage::ThanosClient::DEFAULT_RESPONSE
    include ::Storage::Logging
    attr_reader :options
    attr_accessor :required_headers
    def initialize(options)
      @options = options
      log.level = @options[:log_level]
      @required_headers = {}
    end

    def get(url, opts = {})
      request(Net::HTTP::Get, url, opts)
    end

    def post(url, opts = {})
      opts.update(headers: { 'Content-Type': 'application/json' }) if opts.fetch(
        :body, nil).respond_to?(:[]) && !opts.fetch(:headers, {}).include?('Content-Type')
      request(Net::HTTP::Post, url, opts)
    end

    def put(url, opts = {})
      request(Net::HTTP::Put, url, opts)
    end

    private

    def request(klass, url, opts = {})
      uri = URI(url)
      parameters = opts.fetch(:parameters, {}).transform_keys(&:to_s).transform_values(&:to_s)
      uri.query = URI.encode_www_form(parameters) unless parameters.empty?
      client = Net::HTTP.new(uri.host, uri.port)
      client.use_ssl = (uri.scheme == 'https')
      headers = opts.fetch(:headers, {}).merge(required_headers)
      request = klass.new(uri, headers)
      invoke(client, request, opts)
    end

    # rubocop: disable Metrics/AbcSize
    def invoke(client, request, opts = {})
      body = opts[:body]
      request.body = body.respond_to?(:bytesize) ? body : body.to_json unless body.nil?
      debug_request(request)

      response = ::Storage::ThanosClient::DEFAULT_RESPONSE
      result = {}
      error = nil
      status = response.code
      begin
        response, status = execute(client, request)
      rescue Errno::ECONNREFUSED => e
        log.error e.to_s
        error = e
      rescue EOFError => e
        log.error "Encountered EOF reading from network socket"
        error = e
      rescue OpenSSL::SSL::SSLError => e
        log.error "Socket error: #{e} (#{e.class})"
        error = e
      rescue Net::ReadTimeout => e
        log.error "Timed out reading"
        error = e
      rescue Net::OpenTimeout => e
        log.error "Timed out opening connection"
        error = e
      rescue Net::HTTPBadResponse => e
        log.error e.message
        error = e
        status = e.response.code.to_i if e.respond_to?(:response)
      rescue Net::HTTPUnauthorized => e
        log.error e.message
        error = e
        status = e.response.code.to_i if e.respond_to?(:response)
      rescue Net::HTTPClientException => e
        log.error e.message
        error = e
        status = e.response.code.to_i if e.respond_to?(:response)
      rescue Net::HTTPClientError => e
        log.error "Unexpected HTTP client error: #{e.message} (#{e.class})"
        error = e
        status = e.response.code.to_i if e.respond_to?(:response)
      rescue Net::ProtocolError => e
        log.error "Unexpected HTTP protocol error: #{e.message} (#{e.class})"
        error = e
        status = e.response.code.to_i if e.respond_to?(:response)
      rescue Net::HTTPFatalError => e
        log.error "Unexpected HTTP fatal error: #{e.message} (#{e.class})"
        error = e
      rescue SocketError => e
        log.error "Unexpected socket error: #{e}"
        error = e
      rescue IOError => e
        log.error "Unexpected IO error: #{e} (#{e.class})"
        error = e
      rescue StandardError => e
        log.error "Unexpected error: #{e} (#{e.class})"
        log.error e.exception_type if e.respond_to? :exception_type
        error = e
      end

      headers = {}
      response.each_header { |key, value| headers[key] = value.split(', ') }
      result, error = deserialize(response) if error.nil?

      [result, error, status, headers]
    end
    # rubocop: enable Metrics/AbcSize

    def execute(client, request)
      response = client.request(request)
      log.debug "Response status code: #{response.code}"
      response.value
      [response, response.code.to_i]
    end

    def deserialize(response)
      result = nil
      error = nil
      begin
        response_data = response.body
        result = JSON.parse(response_data)
      rescue JSON::ParserError => e
        n = response.body.length
        message = "Could not parse #{n} bytes of json serialized data from #{uri.path}"
        if n > 65536
          log.warn message
        else
          log.error message
          error = e
        end
      rescue IOError => e
        log.error format('Unexpected IO error: %<error>s (%<error_class>s)', error: e, error_class: e.class)
        error = e
      rescue StandardError => e
        log.error format(
          'Unexpected error: %<error>s (%<error_class>s%<error_type>s)',
          error: e, error_class: e.class,
          error_type: e.respond_to?(:exception_type) ? format('/%s', e.exception_type) : '')
        error = e
      end
      [result, error]
    end
  end
  # class ThanosClient
end
# module Storage

# Re-open the Storage module to define the ShardSelector class
module Storage
  # The ShardSelector class
  class ShardSelector
    include ::Storage::Helpers
    include ::Storage::Logging
    attr_reader :options, :thanos_api_client
    def initialize(options)
      @options = options
      log.level = @options[:log_level]
      @pagination_indices = {}
      @thanos_api_client = Storage::ThanosClient.new(@options)
    end

    def projection(memos, fields)
      memos.map { |memo| memo.slice(*fields) }
    end

    def load_query
      file_path = options[:shards_prometheus_query_file_path]
      IO.read(file_path)
    end

    def fetch_shard_load_ratios
      url = get_api_url(:query_api_uri)
      environment = options[:environment]
      identifiers = options[:prometheus_environment_identifiers]
      identifier = identifiers[environment]
      query = load_query
      query.gsub!('<env>', identifier)

      parameters = {
        query: query,
        time: Time.now.to_f
      }
      log.debug('Querying Thanos for overloaded shards')
      results, error, status, _headers = thanos_api_client.get(url, parameters: parameters)
      raise error unless error.nil?

      raise "Invalid response status code: #{status}" unless [200].include?(status)

      raise "Unexpected response: #{results}" if results.nil? || results.empty?

      results.fetch('data', {}).fetch('result', [])
    end

    def to_app_shard_name(shard)
      return shard.gsub(/.*(\d+)$/, 'praefect-file\1') if shard.include?('praefect')

      format(SHARD_APP_NAME_TEMPLATE, shard: shard.gsub(/-(\d+)$/, '\1'))
    end

    def overloaded_shards
      threshold = options[:threshold]
      shard_load_ratios = fetch_shard_load_ratios
      shard_load_ratios.each_with_object([]) do |item, memo|
        shard = item.fetch('metric', {}).fetch('fqdn', '').gsub(/-stor-g.*$/, '')
        ratio = item.fetch('value', []).last.to_f
        memo << to_app_shard_name(shard) unless ratio < threshold
      end
    end

    def get_shards
      log.debug format('Fetching overloaded shards')
      return options[:simulated_shards] if options[:simulate]

      excluded = options[:excluded]
      shards = overloaded_shards
      shards.reject! { |shard| excluded.include?(shard) }
      shards = shards[0...options[:limit]] if options[:limit].positive?
      shards
    end
  end
  # module ShardSelector
end
# module Storage

# Re-open the Storage module to add ShardSelectorScript module
module Storage
  # ShardSelectorScript module
  module ShardSelectorScript
    include ::Storage::Helpers
    include ::Storage::Logging
    include ::Storage::CommandLineSupport

    def main(args = parse(ARGV, ARGF))
      selector = ::Storage::ShardSelector.new(args)
      shards = selector.get_shards
      puts JSON.pretty_generate(shards: shards)
    rescue UserError => e
      abort e.message
    rescue StandardError => e
      log.error e.message
      e.backtrace.each { |trace| log.error trace }
      abort
    rescue SystemExit
      exit
    rescue Interrupt => e
      $stderr.write "\r\n#{e.class}\n"
      $stderr.flush
      $stdin.echo = true
      exit 0
    end
  end
  # ShardSelectorScript module
end
# Storage module

# Anonymous object avoids namespace pollution
Object.new.extend(::Storage::ShardSelectorScript).main if $PROGRAM_NAME == __FILE__

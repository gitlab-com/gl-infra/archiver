#! /usr/bin/env ruby
# frozen_string_literal: true

# vi: set ft=ruby :

# -*- mode: ruby -*-

require 'csv'
require 'date'
require 'fileutils'
require 'json'
require 'logger'
require 'net/http'
require 'optparse'
require 'ostruct'
require 'time'
require 'uri'

# Storage module
module Storage
  # ProjectSelectorScript module
  module ProjectSelectorScript
    LOG_TIMESTAMP_FORMAT = '%Y-%m-%d %H:%M:%S'
    TWO_WEEKS_AGO_IN_SECONDS = 2 * 7 * 24 * 60 * 60
    ONE_THOUSAND_GIGABYTES = 1_000 * 1024 * 1024 * 1024

    # Config module
    module Config
      DEFAULTS = {
        log_level: Logger::INFO,
        environment: :staging,
        token_env_variable_names: {
          staging: 'GITLAB_GSTG_ADMIN_API_PRIVATE_TOKEN',
          production: 'GITLAB_GPRD_ADMIN_API_PRIVATE_TOKEN'
        },
        api_endpoints: {
          staging: 'https://staging.gitlab.com/api/v4',
          production: 'https://gitlab.com/api/v4'
        },
        projects_api_uri: 'projects',
        projects_per_page: 50,
        projects_last_active_seconds: TWO_WEEKS_AGO_IN_SECONDS,
        project_fields: %i[id path_with_namespace size human_readable_size repository_storage],
        source_shards: [],
        field_renames: {},
        move_amount: ONE_THOUSAND_GIGABYTES,
        limit: -1
      }.freeze
    end
  end
  # module ProjectSelectorScript

  class UserError < StandardError; end
  class Timeout < StandardError; end
end

# Re-open the Storage module to add the Logging module
module Storage
  # This module defines logging methods
  module Logging
    LOG_FORMAT = "%<timestamp>s %-5<level>s %<msg>s\n"

    def formatter_procedure(format_template = LOG_FORMAT)
      proc do |level, t, _name, msg|
        format(
          format_template,
          timestamp: t.strftime(::Storage::ProjectSelectorScript::LOG_TIMESTAMP_FORMAT),
          level: level, msg: msg)
      end
    end

    def initialize_log(formatter = formatter_procedure)
      STDOUT.sync = true
      log = Logger.new(STDOUT)
      log.level = Logger::INFO
      log.formatter = formatter
      log
    end

    def log
      @log ||= initialize_log
    end

    HEADER_ARGUMENT_TEMPLATE = '--header "%<field>s: %<value>s"'
    PRIVATE_TOKEN_HEADER_PATTERN = /Private-Token/i.freeze

    def get_request_headers(request)
      request.instance_variable_get('@header'.to_sym) || []
    end

    # rubocop: disable Style/PercentLiteralDelimiters
    # rubocop: disable Style/RegexpLiteral
    def to_unescaped_json(data)
      return data unless data.respond_to?(:to_json)

      data.to_json.gsub(%r[\\"], '"').gsub(%r[^"{], '{').gsub(%r[}"$], '}')
    end
    # rubocop: enable Style/PercentLiteralDelimiters
    # rubocop: enable Style/RegexpLiteral

    # This method reproduces a Net:HTTP request as a verbatim
    # port to a curl command, executable on the command line,
    # in order to make a given web-request portable.
    def debug_request(request)
      environment = options[:environvment]
      token_env_variable_names = options[:token_env_variable_names]
      env_variable_name = token_env_variable_names[environment]
      headers = get_request_headers(request)
      log.debug "[The following curl command is for external diagnostic purposes only:]"
      curl_command = "curl --verbose --silent --compressed ".dup
      curl_command = curl_command.concat("--request #{request.method.to_s.upcase} ") if request.method != :get
      curl_command = curl_command.concat("'#{request.uri}'")
      header_arguments = headers.collect do |field, values|
        values = [env_variable_name] if PRIVATE_TOKEN_HEADER_PATTERN.match?(field)
        format(HEADER_ARGUMENT_TEMPLATE, field: field, value: values.join(','))
      end
      unless header_arguments.empty?
        curl_command = curl_command.concat(' ')
        curl_command = curl_command.concat(header_arguments.join(' '))
      end
      body = request.body
      curl_command = curl_command.concat(" --data '#{to_unescaped_json(body)}'") unless body.nil? || body.empty?
      log.debug curl_command
    end
  end
  # module Logging
end
# module Storage

# Re-open the Storage module to add the Helpers module
module Storage
  # Helper methods
  module Helpers
    BYTES_CONVERSIONS = {
      'B': 1024,
      'KB': 1024 * 1024,
      'MB': 1024 * 1024 * 1024,
      'GB': 1024 * 1024 * 1024 * 1024,
      'TB': 1024 * 1024 * 1024 * 1024 * 1024
    }.freeze

    def get_api_url(resource_path)
      raise 'No such resource path is configured' unless options.include?(resource_path)

      endpoints = options[:api_endpoints]
      environment = options[:environment]
      endpoint = endpoints.include?(environment) ? endpoints[environment] : endpoints[:production]
      abort 'No api endpoint url is configured' if endpoint.nil? || endpoint.empty?

      [endpoint, options[resource_path]].join('/')
    end

    def symbolize_keys_deep!(memo)
      memo.keys.each do |key|
        symbolized_key = key.respond_to?(:to_sym) ? key.to_sym : key
        memo[symbolized_key] = memo.delete(key) # Preserve order even when key == symbolized_key
        symbolize_keys_deep!(memo[symbolized_key]) if memo[symbolized_key].is_a?(Hash)
      end
    end

    def password_prompt(prompt = 'Enter password: ')
      $stdout.write(prompt)
      $stdout.flush
      $stdin.noecho(&:gets).chomp
    ensure
      $stdin.echo = true
      # $stdout.flush
      $stdout.ioflush
      $stdout.write "\r" + (' ' * prompt.length) + "\n"
      $stdout.flush
    end

    def set_api_token_or_else(&on_failure)
      prompt = options[:password_prompt]
      env_variable_name = options[:token_env_variable_names][options[:environment]]
      token = ENV.fetch(env_variable_name, nil)
      if token.nil? || token.empty?
        log.warn "No #{env_variable_name} variable set in environment"
        token = password_prompt(prompt)
        if token.nil? || token.empty?
          raise 'Failed to get token' unless block_given?

          on_failure.call
        end
      end
      gitlab_api_client.required_headers['Private-Token'] = token
    end

    def to_filesize(bytes)
      BYTES_CONVERSIONS.each_pair do |denomination, threshold|
        return "#{(bytes.to_f / (threshold / 1024)).round(2)} #{denomination}" if bytes < threshold
      end
    end

    def projection(memos, fields)
      memos.map { |memo| memo.slice(*fields) }
    end

    def rename_fields(memos, mapping)
      memos.each do |memo|
        mapping.keys.each do |field|
          if mapping.key?(field)
            new_name = mapping[field]
            memo[new_name] = memo.delete(field)
          end
        end
      end
    end

    def repository_size(project)
      repository_size = project[:size] = project.fetch(:statistics, {}).fetch(:repository_size, 0)
      project[:human_readable_size] = to_filesize(repository_size)
      repository_size
    end

    def limit_by_size(projects, max_amount)
      total_bytes = 0
      projects.each_with_object([]) do |project, memo|
        memo << project if project[:size].positive? && (total_bytes += project[:size]) < max_amount
      end
    end
  end
end

# Re-open the Storage module to add the CommandLineSupport module
module Storage
  # Support for command line arguments
  module CommandLineSupport
    # OptionsParser class
    class OptionsParser
      include ::Storage::Helpers
      attr_reader :parser, :options

      def initialize
        @parser = OptionParser.new
        @options = ::Storage::ProjectSelectorScript::Config::DEFAULTS.dup
        define_options
      end

      def define_options
        @parser.banner = "Usage: #{$PROGRAM_NAME} [options] <source_shard>,..."
        @parser.separator ''
        @parser.separator 'Options:'
        define_head
        define_json_option
        define_move_amount_option
        define_limit_option
        define_env_option
        define_verbose_option
        define_tail
      end

      def define_head
        description = "\nWhere <source_shard> is a list of one or more " \
          "names of source gitaly storage shard servers"
        @parser.on_head(description)
      end

      def define_json_option
        description = 'Absolute path to JSON file enumerating shard names'
        @parser.on('--json=<file_path>', description) do |file_path|
          unless File.exist? file_path
            message = 'Argument given for --json must be a path to an existing file'
            raise OptionParser::InvalidArgument, message
          end

          @options[:source_shards].concat(JSON.parse(IO.read(file_path))['shards'])
        end
      end

      def define_move_amount_option
        description = "Total repository data in gigabytes limit per shard; default: #{@options[:move_amount]}, or " \
          'largest single repo if 0'
        @parser.on('-m', '--move-amount=<n>', Integer, description) do |move_amount|
          abort 'Size too large' if move_amount > 16_000
          # Convert given gigabytes to bytes
          @options[:move_amount] = (move_amount * 1024 * 1024 * 1024)
        end
      end

      def define_limit_option
        limit = @options[:limit]
        default = limit.positive? ? limit : 'no limit'
        description = "Total project limit for all shards; default: #{default}"
        @parser.on('-l', '--limit=<n>', Integer, description) do |limit|
          @options[:limit] = limit
        end
      end

      def validated_env(env)
        env = env.to_sym
        raise OptionParser::ArgumentError, "Invalid environment: #{env}" unless @options[:api_endpoints].key?(env)

        env
      end

      def define_env_option
        description = "Operational environment; default: #{@options[:environment]}"
        @parser.on('--environment=<staging|production>', description) do |env|
          @options[:environment] = validated_env(env)
        end
      end

      def define_verbose_option
        @parser.on('-v', '--verbose', 'Increase logging verbosity') do
          @options[:log_level] -= 1
        end
      end

      def define_tail
        @parser.on_tail('-?', '--help', 'Show this message') do
          puts @parser
          exit
        end
      end
    end
    # class OptionsParser

    def demand(options, arg, type = :positional)
      return options[arg] unless options[arg].nil?

      required_arg = "<#{arg}>"
      required_arg = "--#{arg.to_s.gsub(/_/, '-')}" if type == :optional
      raise UserError, "Required argument: #{required_arg}"
    end

    def parse(args = ARGV, file_path = ARGF)
      opt = OptionsParser.new
      args.push('-?') if args.empty?
      opt.parser.parse!(args)
      # All remaining arguments should be considered source shards
      # specifications
      opt.options[:source_shards].concat(args)
      opt.options
    rescue OptionParser::InvalidArgument, OptionParser::InvalidOption,
           OptionParser::MissingArgument, OptionParser::NeedlessArgument => e
      puts e.message
      puts opt.parser
      exit
    rescue OptionParser::AmbiguousOption => e
      abort e.message
    end
  end
  # module CommandLineSupport
end
# module Storage

# Re-open the Storage module to define the GitLabClient class
module Storage
  # Define the GitLabClient class
  class GitLabClient
    DEFAULT_RESPONSE = OpenStruct.new(code: 400, body: '{}') unless defined? ::Storage::GitLabClient::DEFAULT_RESPONSE
    include ::Storage::Logging
    attr_reader :options
    attr_accessor :required_headers
    def initialize(options)
      @options = options
      log.level = @options[:log_level]
      @required_headers = {}
    end

    def get(url, opts = {})
      request(Net::HTTP::Get, url, opts)
    end

    def post(url, opts = {})
      opts.update(headers: { 'Content-Type': 'application/json' }) if opts.fetch(
        :body, nil).respond_to?(:[]) && !opts.fetch(:headers, {}).include?('Content-Type')
      request(Net::HTTP::Post, url, opts)
    end

    def put(url, opts = {})
      request(Net::HTTP::Put, url, opts)
    end

    private

    def request(klass, url, opts = {})
      uri = URI(url)
      parameters = opts.fetch(:parameters, {}).transform_keys(&:to_s).transform_values(&:to_s)
      uri.query = URI.encode_www_form(parameters) unless parameters.empty?
      client = Net::HTTP.new(uri.host, uri.port)
      client.use_ssl = (uri.scheme == 'https')
      headers = opts.fetch(:headers, {}).merge(required_headers)
      request = klass.new(uri, headers)
      invoke(client, request, opts)
    end

    # rubocop: disable Metrics/AbcSize
    def invoke(client, request, opts = {})
      body = opts[:body]
      request.body = body.respond_to?(:bytesize) ? body : body.to_json unless body.nil?
      debug_request(request)

      response = ::Storage::GitLabClient::DEFAULT_RESPONSE
      result = {}
      error = nil
      status = response.code
      begin
        response, status = execute(client, request)
      rescue Errno::ECONNREFUSED => e
        log.error e.to_s
        error = e
      rescue EOFError => e
        log.error "Encountered EOF reading from network socket"
        error = e
      rescue OpenSSL::SSL::SSLError => e
        log.error "Socket error: #{e} (#{e.class})"
        error = e
      rescue Net::ReadTimeout => e
        log.error "Timed out reading"
        error = e
      rescue Net::OpenTimeout => e
        log.error "Timed out opening connection"
        error = e
      rescue Net::HTTPBadResponse => e
        log.error e.message
        error = e
        status = e.response.code.to_i if e.respond_to?(:response)
      rescue Net::HTTPUnauthorized => e
        log.error e.message
        error = e
        status = e.response.code.to_i if e.respond_to?(:response)
      rescue Net::HTTPClientException => e
        log.error e.message
        error = e
        status = e.response.code.to_i if e.respond_to?(:response)
      rescue Net::HTTPClientError => e
        log.error "Unexpected HTTP client error: #{e.message} (#{e.class})"
        error = e
        status = e.response.code.to_i if e.respond_to?(:response)
      rescue Net::ProtocolError => e
        log.error "Unexpected HTTP protocol error: #{e.message} (#{e.class})"
        error = e
        status = e.response.code.to_i if e.respond_to?(:response)
      rescue Net::HTTPFatalError => e
        log.error "Unexpected HTTP fatal error: #{e.message} (#{e.class})"
        error = e
      rescue IOError => e
        log.error "Unexpected IO error: #{e} (#{e.class})"
        error = e
      rescue StandardError => e
        log.error "Unexpected error: #{e} (#{e.class})"
        log.error e.exception_type if e.respond_to? :exception_type
        error = e
      end

      headers = {}
      response.each_header { |key, value| headers[key] = value.split(', ') }
      result, error = deserialize(response) if error.nil?

      [result, error, status, headers]
    end
    # rubocop: enable Metrics/AbcSize

    def execute(client, request)
      response = client.request(request)
      log.debug "Response status code: #{response.code}"
      response.value
      [response, response.code.to_i]
    end

    def deserialize(response)
      result = nil
      error = nil
      begin
        response_data = response.body
        result = JSON.parse(response_data)
      rescue JSON::ParserError => e
        n = response.body.length
        message = "Could not parse #{n} bytes of json serialized data from #{uri.path}"
        if n > 65536
          log.warn message
        else
          log.error message
          error = e
        end
      rescue IOError => e
        log.error format('Unexpected IO error: %<error>s (%<error_class>s)', error: e, error_class: e.class)
        error = e
      rescue StandardError => e
        log.error format(
          'Unexpected error: %<error>s (%<error_class>s%<error_type>s)',
          error: e, error_class: e.class,
          error_type: e.respond_to?(:exception_type) ? format('/%s', e.exception_type) : '')
        error = e
      end
      [result, error]
    end
  end
  # class GitLabClient
end
# module Storage

# Re-open the Storage module to define the ProjectSelector class
module Storage
  # The ProjectSelector class
  class ProjectSelector
    include ::Storage::Helpers
    include ::Storage::Logging
    attr_reader :options, :gitlab_api_client
    def initialize(options)
      @options = options
      log.level = @options[:log_level]
      @pagination_indices = {}
      @gitlab_api_client = Storage::GitLabClient.new(@options)
    end

    def get_page(url, parameters)
      message = 'Fetching largest projects page %<page>d'
      log.debug format(message, page: parameters.fetch('page', 1))
      results, error, status, headers = gitlab_api_client.get(url, parameters: parameters)
      raise error unless error.nil?

      raise "Invalid response status code: #{status}" unless [200].include?(status)

      raise "Unexpected response: #{results}" if results.nil? || results.empty?

      results.each { |result| symbolize_keys_deep!(result) }
      log.debug JSON.pretty_generate(results)
      [results, headers['x-next-page'].first]
    end

    def pagination_key(url, identifier)
      url + '[' + identifier + ']'
    end

    def fetch_largest_projects(shard)
      log.debug "Fetching largest projects for shard: #{shard}"
      projects = []
      url = get_api_url(:projects_api_uri)
      last_active = (Time.now.utc - options[:projects_last_active_seconds]).iso8601
      projects_per_page = options[:projects_per_page]
      limit = options[:limit]
      per_page = limit.positive? ? [limit, projects_per_page].min : projects_per_page
      parameters = {
        order_by: 'repository_size',
        last_activity_before: last_active,
        statistics: true,
        repository_storage: shard,
        per_page: per_page
      }
      pagination_key = pagination_key(url, shard)
      next_page = @pagination_indices[pagination_key]
      if next_page != :no_more_pages
        parameters['page'] = next_page unless next_page.nil?
        projects, next_page = get_page(url, parameters)
        @pagination_indices[pagination_key] = next_page || :no_more_pages
      end
      projects
    end

    def get_projects_from_shard(shard)
      project_fields = options[:project_fields]
      field_renames = options[:field_renames]
      limit = options[:limit]
      move_amount = options[:move_amount]
      projects = []
      total_bytes = 0
      loop do
        page_of_projects = fetch_largest_projects(shard)
        break if page_of_projects.empty?

        projects.concat(page_of_projects)
        total_bytes += page_of_projects.sum(0) { |project| repository_size(project) }
        break if total_bytes >= move_amount || (limit.positive? && projects.length > limit)
      end
      projects = limit_by_size(projects, move_amount)
      projects = projection(projects, project_fields)
      rename_fields(projects, field_renames)
      projects
    end

    def get_projects
      source_shards = options[:source_shards]
      limit = options[:limit]
      projects = source_shards.each_with_object([]) do |source_shard, memo|
        memo.concat(get_projects_from_shard(source_shard)) if limit.negative? || memo.length < limit
      end
      projects = projects[0...options[:limit]] unless limit.negative?
      projects
    end
  end
  # module CommandLineSupport
end
# module Storage

# Re-open the Storage module to add ProjectSelectorScript module
module Storage
  # ProjectSelectorScript module
  module ProjectSelectorScript
    include ::Storage::Helpers
    include ::Storage::Logging
    include ::Storage::CommandLineSupport

    def main(args = parse(ARGV, ARGF))
      demand(args, :source_shards)
      selector = ::Storage::ProjectSelector.new(args)
      selector.set_api_token_or_else do
        raise UserError, 'Cannot proceed without a GitLab admin API private token'
      end
      projects = selector.get_projects
      puts JSON.pretty_generate(projects: projects)
    rescue UserError => e
      abort e.message
    rescue StandardError => e
      log.error e.message
      e.backtrace.each { |trace| log.error trace }
      abort
    rescue SystemExit
      exit
    rescue Interrupt => e
      $stderr.write "\r\n#{e.class}\n"
      $stderr.flush
      $stdin.echo = true
      exit 0
    end
  end
  # ProjectSelectorScript module
end
# Storage module

# Anonymous object avoids namespace pollution
Object.new.extend(::Storage::ProjectSelectorScript).main if $PROGRAM_NAME == __FILE__
